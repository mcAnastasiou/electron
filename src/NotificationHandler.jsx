import React, { Component } from 'react'

export default class NotificationHandler 
extends Component {
    constructor(props) {
        super(props);
        this.state = {
            message: '',
        }

        this.sendNotification = this.sendNotification.bind(this);
    }

    sendNotification() {
        let myNotification = new Notification('Title', {
            body: 'Lorem Ipsum Dolor Sit Amet'
          })
          
          myNotification.onclick = () => {
            this.setState({
                message: 'Notication clicked',
            })
         }
    }

    render() {
        const { message } = this.state;
        return (
            <div>
                <h2>{message}</h2>
                <button onClick={this.sendNotification}>Send notification</button>            
            </div>
        )
    }
}
