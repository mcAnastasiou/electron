import React from 'react';
import logo from './logo.svg';
import './App.css';
import NotificationHandler from './NotificationHandler';

function App() {
  
  return (
    <div>
      <h1>Have fun</h1>
      <p>notification test</p>
      <NotificationHandler />
    </div>
  );
}

export default App;
